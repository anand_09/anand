package com.sapient.database;

import java.sql.*;
import java.util.ArrayList;

public class CricketDAO {
	public ArrayList<PlayerBean> getPlayers() throws Exception {
		Connection co = DbConn.getMyConnection();
		PreparedStatement ps = co.prepareStatement("select id,firstname,lastname,jerseynum from players");
		ResultSet rs = ps.executeQuery(); 
		ArrayList<PlayerBean> list = new ArrayList<PlayerBean>();    
		PlayerBean player = null;    
		while(rs.next()) {          
			player = new PlayerBean(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4));    
			list.add(player);
		}    
		return list;
	}
	
	public ArrayList<PlayerBean> getAnyPlayer(int id) throws Exception {
		Connection co = DbConn.getMyConnection();
		PreparedStatement ps = co.prepareStatement("select id,firstname,lastname,jerseynum from players where ID = ?");
		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery(); 
		ArrayList<PlayerBean> list = new ArrayList<>();    
		PlayerBean player = null;    
		while(rs.next()) {          
			player = new PlayerBean(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4));    
			list.add(player);
		}    
		co.close();
		return list;
	}
	
	public void insertPlayer(PlayerBean pb) throws Exception {
		Connection co = DbConn.getMyConnection();
		PreparedStatement ps = co.prepareStatement("INSERT INTO PLAYERS VALUES(?,?,?,?)");
		ps.setInt(1, pb.getId());
		ps.setString(2, pb.getFirstName());
		ps.setString(3, pb.getLastName());
		ps.setInt(4, pb.getJerseyNo());
		int rs = ps.executeUpdate(); 
		System.out.println(rs+" Players Added");
		co.close();
	}
	
	public void deletePlayer(int id) throws Exception {
		Connection co = DbConn.getMyConnection();
		PreparedStatement ps = co.prepareStatement("DELETE FROM PLAYERS WHERE ID = ?");
		ps.setInt(1, id);
		int rs = ps.executeUpdate(); 
		System.out.println(rs+" Players Deleted");
		co.close();
	}
	
	public int updatePlayer(int id, PlayerBean pb) throws Exception {
		Connection co = DbConn.getMyConnection();
		PreparedStatement ps = co.prepareStatement("UPDATE PLAYERS SET FIRSTNAME=?,LASTNAME=?,JERSEYNUM=? WHERE ID=?");
		ps.setString(1, pb.getFirstName());
		ps.setString(2, pb.getLastName());
		ps.setInt(3, pb.getJerseyNo());
		ps.setInt(4, id);
		int rs = ps.executeUpdate(); 
		System.out.println(rs+" Players Added");
		co.close();
		return rs;
	}
	
}
