package com.sapient.database;

public class PlayerBean {
	private int Id;
	private String FirstName;
	private String LastName;
	private int JerseyNo;
	
	public String toString() {
		return "PlayerBean[ Id "+Id+" FirstName "+FirstName+" LastName "+LastName+ " JerseyNo "+JerseyNo+" ]";
	}
	
	public PlayerBean(int Id, String fn, String ln, int j) {
		super();
		this.Id = Id;
		this.FirstName = fn;
		this.LastName = ln;
		this.JerseyNo = j;
	}
	
	public PlayerBean() {
		super();
	}
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public int getJerseyNo() {
		return JerseyNo;
	}
	public void setJerseyNo(int jerseyNo) {
		JerseyNo = jerseyNo;
	}
	
}
