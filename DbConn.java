package com.sapient.database;

import java.sql.*;


public class DbConn {
	public static Connection myConnection = null; 
	
	public static Connection getMyConnection() throws Exception {
		if(myConnection == null) {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			myConnection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "Balbeer9chess");
		}
		return myConnection;
	}
	
}
