package com.sapient.week1;

public class Matrix {
	int ar[][];
	
	Matrix() {
		ar = new int[3][3];
	}
	
	Matrix(int m, int n) {
		ar = new int[m][n];
	}
	
	Matrix(Matrix m) {
		this.ar = m.ar;
	}
	
	Matrix(int m[][]) {
		ar = new int[3][3];
	}
	
	public void read() {
		System.out.println("Enter row " + ar.length + " and columns " + ar[0].length);
		for(int i=0;i<ar.length;i++) {
			for(int j=0;j<ar[i].length;j++)
				ar[i][j] = Read.sc.nextInt();
		}
	}
	
	public void display() {
		for(int i=0;i<ar.length;i++) {
			for(int j=0;j<ar[i].length;j++)
				System.out.print(ar[i][j] + " ");
			System.out.println();
		}
	}
	
	public void add(Matrix m) {
		if(ar.length != m.ar.length || ar[0].length != m.ar[0].length) {
			System.out.println("Addition Not possible");
		}
		else {
			Matrix ma = new Matrix();
			ma.ar = new int[ar.length][ar[0].length];
			for(int i=0;i<ar.length;i++) { 
				for(int j=0;j<ar[i].length;j++)
					ma.ar[i][j] = m.ar[i][j] + ar[i][j];
			}
			ma.display();
		}
	}
	
	public void subtract(Matrix m) {
		if(ar.length != m.ar.length || ar[0].length != m.ar[0].length) {
			System.out.println("Subtraction Not possible");
		}
		else {
			Matrix ma = new Matrix();
			ma.ar = new int[ar.length][ar[0].length];
			for(int i=0;i<ar.length;i++) { 
				for(int j=0;j<ar[i].length;j++)
					ma.ar[i][j] = ar[i][j] - m.ar[i][j];
			}
			ma.display();
		}
	}
	
	public void multiply(Matrix m) {
		if(ar[0].length != m.ar.length) {
			System.out.println("Multiplication Not possible");
		}
		else {
			Matrix ma = new Matrix();
			ma.ar = new int[ar.length][m.ar[0].length];
			for(int i=0;i<m.ar[0].length;i++) { 
		        for(int j=0;j<m.ar.length;j++) { 
		            for(int k=0;k<m.ar.length;k++) {
		            	ma.ar[i][j] += ar[i][k] *  m.ar[k][j];
		            }
		        } 
		    } 
			ma.display();
		}
	}

}
